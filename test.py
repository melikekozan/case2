from couchbase.bucket import Bucket
from couchbase.n1ql import N1QLQuery

# Connect to Couchbase
bucket = Bucket('http://localhost/bootcamp')

# Upsert a document in the bucket
bucket.upsert("movie1", {
  "movieName": "Her Şey Çok Güzel Olacak",
  "kind": "comedy",
})

print(bucket.get("movie1").value)

query = N1QLQuery("SELECT movieName, kind FROM `bootcamp`")
for row in bucket.n1ql_query(query):
    print(row)